import { LogstashTCP } from '../LogstashTCP';
require('dotenv').config();

const defaultPayload = {
  app_name: 'Unit tests',
  app_port: -1,
};

test('Send simple log', async () => {
  const tcp = new LogstashTCP(process.env.HOST_NAME!, parseInt(process.env.HOST_PORT!));
  tcp.defaultPayload = defaultPayload;

  expect(tcp.sendMessage('Sample log message')).toBeTruthy();

  tcp.close();
});

test('Send complex log', async () => {
  const tcp = new LogstashTCP(process.env.HOST_NAME!, parseInt(process.env.HOST_PORT!));
  tcp.defaultPayload = defaultPayload;

  expect(
    tcp.send({
      ...defaultPayload,
      message: 'Weapons ready',
      level_value: 20000,
      thread_name: 'SHIELD-HelicarrierWeaponsSystem',
      level: 'INFO',
      host: 'shd-hel-616',
      '@version': '1',
      logger_name: 'gov.shield.helicarrier.WeaponsSystem',
    }),
  ).toBeTruthy();

  tcp.close();
});
