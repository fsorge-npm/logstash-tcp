import net = require('net');

/**
 * Send logs via TCP to Logstash
 * @link Examples https://www.npmjs.com/package/logstash-tcp#examples
 * @author Francesco Sorge <contact@francescosorge.com>
 */
export class LogstashTCP {
  private readonly _connection: net.Socket;
  public defaultPayload: { [key: string]: any } = {};
  public customMessageFieldName = 'message';

  /**
   * Opens a TCP connection to Logstash for sending data
   * @param host hostname or IP to Logstash instance
   * @param port destination port to Logstash instance
   * @throws `Error` when hostname or port are incorrect
   */
  public constructor(host: string, port: number) {
    this._connection = net.createConnection({ host, port }).on('error', (e) => {
      throw new Error(`LogstashTCP encountered a connection error to Logstash instance ${JSON.stringify(e)}`);
    });
    this._connection.setNoDelay(true);
  }

  /**
   * Sends a payload to Logstash
   * @param payload a JSON payload
   * @returns `true` if sent successfully, `false` otherwise
   * @throws `Error` if something goes too bad even for returning a falsy response
   */
  public send(payload: { [key: string]: any }): boolean {
    return this._connection.write(
      JSON.stringify({
        ...this.defaultPayload,
        ...payload,
      }) + '\n',
      (e) => {
        if (e) {
          throw new Error(`LogstashTCP encountered a while sending to Logstash instance ${JSON.stringify(e)}`);
        }
      },
    );
  }

  /**
   * Sends a message to Logstash
   * @param message a message that will be automatically be converted in a valid Logstash payload
   * @returns `true` if sent successfully, `false` otherwise
   * @throws `Error` if something goes too bad even for returning a falsy response
   */
  public sendMessage(message: string): boolean {
    return this.send({
      ...this.defaultPayload,
      [this.customMessageFieldName]: message,
    });
  }

  /**
   * Safely closes the connection
   */
  public close() {
    this._connection.end();
  }

  /**
   * Immediately (and brutally) closes the connection
   */
  public destroy() {
    this._connection.destroy();
  }
}

export default LogstashTCP;
