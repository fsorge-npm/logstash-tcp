<img alt="Quality Gate Status" src="https://sonarcloud.io/api/project_badges/measure?project=logstash-tcp&metric=alert_status"/>

<img alt="Maintainability Rating" src="https://sonarcloud.io/api/project_badges/measure?project=logstash-tcp&metric=sqale_rating"/>

<img alt="Reliability Rating" src="https://sonarcloud.io/api/project_badges/measure?project=logstash-tcp&metric=reliability_rating"/>

<img alt="Security Rating" src="https://sonarcloud.io/api/project_badges/measure?project=logstash-tcp&metric=security_rating"/>

<img alt="Bugs" src="https://sonarcloud.io/api/project_badges/measure?project=logstash-tcp&metric=bugs"/>

<img alt="Vulnerabilities" src="https://sonarcloud.io/api/project_badges/measure?project=logstash-tcp&metric=vulnerabilities"/>

<img alt="Coverage" src="https://sonarcloud.io/api/project_badges/measure?project=logstash-tcp&metric=coverage"/>

<img alt="Pipeline" src="https://img.shields.io/gitlab/pipeline/fsorge-npm/logstash-tcp/main"/>

# LogstashTCP

A simple library to send logs and messages to a Logstash instance via NodeJS and TCP.


## Quick start

1. Add LogstashTCP as a dependency of your project. It's as simple as:
```bash
npm i logstash-tcp
```

2. Import the library
```ts
import { LogstashTCP } from 'logstash-tcp';
```

3. Start sending logs

4. Remember to close connection when exiting your application


## Examples

### Connection

```javascript
const tcp = new LogstashTCP('localhost', 5005);
```

### Send to Logstash

```javascript
tcp.sendMessage('Sample log message');
```

### Send complex objects

```javascript
tcp.send({
    "message": "Weapons ready",
    "level_value": 20000,
    "thread_name": "SHIELD-HelicarrierWeaponsSystem",
    "level": "INFO",
    "host": "shd-hel-616",
    "@version": "1",
    "app_name": "unit-test",
    "app_port": "-1",
    "logger_name": "gov.shield.helicarrier.WeaponsSystem"
  });
```

### Close the connection

This will wait for all queued logs to be sent before closing

```javascript
tcp.close();
```

### Destroy the connection

This will close the connection immediately and will not send queued logs

```javascript
tcp.destroy();
```


## Custom payload

If you have fields that are always sent to Logstash, you can define a
custom payload which gets sent everytime you send something.

```javascript
const defaultPayload = {
  app_name: 'My super app',
  app_port: 8080
};
tcp.defaultPayload = defaultPayload;
tcp.sendMessage('Sample log message');
```

This example will send the following payload
```json
{
    app_name: 'My super app',
    app_port: 8080,
    message: 'Sample log message'
}
```


## Default message field name

If you have a custom name for the 'message' field, you can easely specify it

```javascript
tcp.customMessageFieldName = '@message';
tcp.sendMessage('Sample log message');
```

This example will send the following payload
```json
{
    '@message': 'Sample log message'
}
```
  

## Develop

Write code and run tests against it with `npm test`.

Note: you need a valid .env file to run tests as it needs a working Logstash or a mock server.